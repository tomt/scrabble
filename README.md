# Scrabble AI

A Level Computer Science NEA project, including:

- A real-time online multiplayer browser game
- Historical player statistics
- AI opponent

# Project structure

- `crates/ai`: Move generator and AI.
- `crates/api`: Rust data types which model all communication between the client and server with a single, compile-time checked, definition.
- `crates/auth`: Server-side authentication/authorization implementation.
- `crates/cli`: Server command line interface.
- `crates/client`: Communicates with the server over HTTP (& WebSockets).
- `crates/db`: Server database implementation.
- `crates/fsm`: Deduplicated finite state machine implementations.
- `crates/game`: Game state representation, move validation & scoring.
- `crates/mail`: Server-side email sending.
- `crates/server`: Backend, written with `axum` & `sqlx`.  Runs on a seperate machine and persists user/game data, manages multiplayer games etc.  Enforces legality of plays.  Static files for the SPA should be served by the reverse proxy.
- `crates/ui`: WebAssembly SPA written with `dioxus`. 
