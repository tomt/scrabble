//! Module for performing authorization.

pub mod hex;
pub mod validation;

mod error;
mod jwt;
mod password;

pub use error::{Error, Result};
pub use jwt::{Jwt, Role};
pub use password::{hash, verify};

const BEARER: &str = "Bearer ";

pub async fn validate(role: Role, bearer_header: &str) -> Result<Jwt> {
    log::info!("validating auth");

    if !bearer_header.starts_with(BEARER) {
        return Err(Error::InvalidBearerHeader);
    }

    let token = bearer_header.trim_start_matches(BEARER).trim_end();
    let jwt = Jwt::from_auth_token(token, role)?;

    log::info!("authenticated: {}", jwt.id_user());

    Ok(jwt)
}
