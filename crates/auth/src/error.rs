//! Error types for auth module.

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug, Clone, Copy)]
pub enum Error {
    InvalidBearerHeader,
    InvalidUsername,
    InvalidPassword,
    InvalidEmail,
    JwtDecode,
    JwtEncode,
    IncorrectRole,
    IncorrectPassword,
}
