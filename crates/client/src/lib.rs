//! Methods for interacting with the API asynchronously.

use reqwest::{Method, Request, Url};
use scrabble_api::{
    auth::{AuthWrapper, Token},
    routes::users::UserDetails,
};
use serde::{de::DeserializeOwned, Deserialize, Serialize};

mod error;
pub mod friends;
pub mod games;
pub mod leaderboard;
pub mod live;
pub mod users;

pub use error::{Error, Result};

/// The auth data, contains user info and the auth token.
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct AuthContext {
    /// Username and email
    pub user_details: UserDetails,
    /// JWT from server
    pub token: Token,
}

/// The domain name and path to the API, excluding the protocol.
pub const API_HOST: &str = "thrgd.uk/api";

/// Make a request to the path {API_URL}/{url}, with the provided
/// method and data. Returns the optional auth from the server and
/// the requested value.
pub async fn request<T, U>(
    url: &str,
    method: Method,
    data: Option<&T>,
    auth: Option<&AuthContext>,
) -> Result<(Option<Token>, U)>
where
    T: Serialize,
    U: DeserializeOwned,
{
    let url = Url::parse(&format!("https://{API_HOST}{url}"))?;
    let mut req = Request::new(method, url);

    // Add the JSON body.
    if let Some(data) = data {
        let body = serde_json::to_string(data)?;
        req = req.header("Content-Type", "Application/JSON").body(body);
        log::debug!("added json body");
    }

    // Add the auth header
    if let Some(auth_signal) = auth {
        if let Some(AuthContext { token, .. }) = auth_signal.get().as_ref() {
            let Token(token) = token;
            req = req.header("Authorization", &format!("Bearer {token}"));
            log::debug!("added auth header");
        }
    }

    log::info!("sending request: {req:#?}");

    let response = req.send().await?;

    log::info!("response received: {response:?}");

    // match on the response http status and return either
    // an error message or the deserialized content.
    match response.status() {
        // (200 OK) or (201 CREATED)
        200 | 201 => Ok({
            // attempt to deserialize as `AuthWrapper<U>`.
            if let Ok(AuthWrapper { token, response }) = response.json().await {
                (token, response)
            }
            // attempt to deserialize as `U`.
            else {
                log::error!("failed to parse successful response");
                (None, response.json().await?)
            }
        }),
        status => {
            if let Ok(error_response) = response.json().await {
                Err(Error::Api(error_response))
            } else {
                log::error!("failed to parse error response");
                Err(Error::HttpStatus(status))
            }
        }
    }
}

/// Makes a request and updates the auth field if a token is
/// received.
pub async fn req_std<T, U>(
    url: &str,
    method: Method,
    data: Option<&T>,
    auth: Option<&AuthContext>,
) -> Result<U>
where
    T: Serialize,
    U: DeserializeOwned,
{
    let (token, response) = request(url, method, data, auth).await?;

    // If a new auth token is received, update the stored token.
    if let Some(token) = token {
        if let Some(auth) = auth {
            set_token(auth, token);
        }
    }

    Ok(response)
}

/// Makes a standard request with an empty body.
pub async fn req_no_body<U>(url: &str, method: Method, auth: Option<&AuthContext>) -> Result<U>
where
    U: DeserializeOwned,
{
    req_std(url, method, Option::<&()>::None, auth).await
}
