use crate::{error::Result, API_HOST};
use futures::SinkExt;
use scrabble_api::{auth::Token, routes::live::ClientMsg};
use tokio_tungstenite::connect_async;

/// Connects to the live websocket server and authenticates
/// the user.
pub async fn connect_and_authenticate(token: Token) -> Result<WebSocket> {
    let wss_url = format!("wss://{API_HOST}/live");
    let (ws, _) = tokio_tungstenite::connect_async(&wss_url).await?;

    // Send a `ClientMsg::Auth` to authenticate the connection.
    ws.send(to_msg(&ClientMsg::Auth(token))).await?;

    Ok(ws)
}

/// Converts a `ClientMsg` to a websocket message.
pub fn to_msg(msg: &ClientMsg) -> Message {
    let bytes = bincode::serialize(msg).unwrap();
    Message::Bytes(bytes)
}
