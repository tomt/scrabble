//! Convenience methods for the games api route.

use crate::{error::Result, req_no_body, AuthContext};
use reqwest::Method;
use scrabble_api::routes::games::*;

/// GET /api/games [+Auth]
pub async fn list(auth: &AuthContext) -> Result<ListGamesResponse> {
    req_no_body("/games", Method::GET, Some(auth)).await
}

/// GET /api/games/{game id}/stats [+Auth]
pub async fn stats(auth: &AuthContext, id_game: i32) -> Result<GameStatsResponse> {
    req_no_body(&format!("/games/{id_game}/stats"), Method::GET, Some(auth)).await
}

/// GET /api/games/stats [+Auth]
pub async fn overall_stats(auth: &AuthContext) -> Result<OverallStatsResponse> {
    req_no_body("/games/stats", Method::GET, Some(auth)).await
}
