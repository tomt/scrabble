//! Convenience methods for the friends api route.

use crate::{error::Result, req_no_body, AuthContext};
use reqwest::Method;
use scrabble_api::routes::friends::*;

/// POST /api/friends [+Auth]
pub async fn add(auth: &AuthContext, username: String) -> Result<()> {
    req_no_body(&format!("/friends/{username}"), Method::POST, Some(auth)).await
}

/// DELETE /api/friends/{username} [+Auth]
pub async fn remove(auth: &AuthContext, username: String) -> Result<()> {
    req_no_body(&format!("/friends/{username}"), Method::DELETE, Some(auth)).await
}

/// GET /api/friends [+Auth]
pub async fn list(auth: &AuthContext) -> Result<FriendsResponse> {
    req_no_body("/friends", Method::GET, Some(auth)).await
}

/// GET /api/friends/requests/incoming [+Auth]
pub async fn list_incoming(auth: &AuthContext) -> Result<FriendRequestsResponse> {
    req_no_body("/friends/requests/incoming", Method::GET, Some(auth)).await
}

/// GET /api/friends/requests/outgoing [+Auth]
pub async fn list_outgoing(auth: &AuthContext) -> Result<FriendRequestsResponse> {
    req_no_body("/friends/requests/outgoing", Method::GET, Some(auth)).await
}
