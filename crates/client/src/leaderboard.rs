//! Convenience methods for the leaderboard api route.

use crate::{error::Result, req_no_body, AuthContext};
use reqwest::Method;
use scrabble_api::routes::leaderboard::*;

/// GET /api/leaderboard
pub async fn overall_leaderboard(count: usize, offset: usize) -> Result<LeaderboardResponse> {
    req_no_body(
        &format!("/leaderboard?count={count}&offset={offset}"),
        Method::GET,
        None,
    )
    .await
}

/// GET /api/leaderboard/friends
pub async fn friends_leaderboard(auth: &AuthContext) -> Result<LeaderboardResponse> {
    req_no_body("/leaderboard/friends", Method::GET, Some(auth)).await
}
