use super::tile::Tile;
use dioxus::prelude::*;
use scrabble_game::{
    game::tile,
    util::pos::{Pos, Premium},
};

/// The class used to style squares with a bonus.
fn square_class(pos: Pos) -> &'static str {
    match pos.premium() {
        None => "square",
        Some(Premium::DoubleLetter) => "square double-letter",
        Some(Premium::DoubleWord) => "square double-word",
        Some(Premium::Start) => "square start",
        Some(Premium::TripleLetter) => "square triple-letter",
        Some(Premium::TripleWord) => "square triple-word",
    }
}

fn square_contents(pos: Pos) -> &'static str {
    match pos.premium() {
        None => "",
        Some(Premium::DoubleLetter) => "2L",
        Some(Premium::DoubleWord) => "2W",
        Some(Premium::Start) => "S",
        Some(Premium::TripleLetter) => "3L",
        Some(Premium::TripleWord) => "3W",
    }
}

/// Props for `Board`.
#[derive(Prop)]
pub struct BoardProps<'a, F> {
    /// A function of the position that was clicked.
    pub on_click: F,
    /// The Option<Tile> array for the board.
    pub cells: &'a ReadSignal<Vec<Option<tile::Tile>>>,
}

/// View the scrabble board, providing a single dimensional array containing
/// the 225 optional tiles.
#[component]
pub fn Board<'a, F, G: Html>(cx: Scope<'a>, props: BoardProps<'a, F>) -> View<G>
where
    F: Fn(Pos) + Clone + 'a,
{
    let on_click = create_ref(cx, props.on_click);
    let squares = create_memo(cx, move || {
        let cells = props.cells.get();
        let cells = cells.as_ref();

        View::new_fragment(
            Pos::iter()
                .zip(cells)
                .map(|(p, t)| (p, *t))
                .map(|(pos, tile)| {
                    let on_click = on_click.clone();
                    let on_click = move |_| {
                        let on_click = on_click.clone();
                        on_click(pos);
                    };

                    rsx! {
                        div { class: "{square_class(pos)}",
                            onclick: on_click,
                            
                            (match tile {
                                Some(tile @ tile::Tile::Letter { .. }) => view! { cx, Tile { tile } },
                                Some(tile @ tile::Tile::Blank(Some(letter))) => view! { cx,
                                    Tile { tile }

                                    div { class: "blank-letter", "{letter}" }
                                },
                                _ => view! { cx,
                                    div { class: "premium", "{square_contents(pos)}" }
                                }
                            })
                        }
                    }
                })
                .collect(),
        )
    });

    view! { cx,
        div(class="board") {
            (*squares.get())
        }
    }
}
