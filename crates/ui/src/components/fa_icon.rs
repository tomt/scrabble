//! A FontAwesome icon.

use dioxus::prelude::*;

/// Component for consistent font-awesome icons. Applies a
/// margin to the left and right of the icon. The `class` prop
/// is used to set the icon.
#[component]
pub fn FaIcon(class: String) -> Element {
    rsx! {
        span { class: "mx-1",
            i { class }
        }
    }
}
