//! A database pool that can be shared across threads.

use sqlx::sqlite::{SqlitePool, SqlitePoolOptions};

mod error;
mod models;

pub use error::{Error, Result};

/// Thread safe database pool.
pub type Db = SqlitePool;

/// Connects to the database url in $DATABASE_URL.
pub async fn connect(database_url: &str) -> Result<Db> {
    log::info!("connecting to database: {database_url}");
    let pool = SqlitePoolOptions::new()
        .max_connections(10)
        .connect(database_url)
        .await?;

    Ok(pool)
}
