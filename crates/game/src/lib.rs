//! Implementation of a multiplayer scrabble game.

// Produce a compiler warning for missing documentation.
#![warn(missing_docs)]

pub mod error;
pub mod game;
pub mod util;
