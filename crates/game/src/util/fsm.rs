//! Implements `FsmSequence<Letter>` for various types.

use std::{iter, str::Chars};

use scrabble_fsm::FsmSequence;

use crate::game::tile::Letter;

/// Used to iterate over the Symbols in a string.
pub struct FsmCharsIter<'a> {
    inner: Chars<'a>,
}

impl<'a> Iterator for FsmCharsIter<'a> {
    type Item = Letter;

    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next().and_then(Letter::new)
    }
}
impl<'a> FsmSequence<Letter> for &'a str {
    type Iter = FsmCharsIter<'a>;

    fn into_iter(self) -> Self::Iter {
        FsmCharsIter {
            inner: self.chars(),
        }
    }
}
impl<'a> FsmSequence<Letter> for &'a String {
    type Iter = FsmCharsIter<'a>;

    fn into_iter(self) -> Self::Iter {
        FsmCharsIter {
            inner: self.chars(),
        }
    }
}
impl<'a> FsmSequence<Letter> for &'a [Letter] {
    type Iter = iter::Copied<std::slice::Iter<'a, Letter>>;

    fn into_iter(self) -> Self::Iter {
        self.iter().copied()
    }
}
impl FsmSequence<Letter> for Letter {
    type Iter = iter::Once<Letter>;

    fn into_iter(self) -> Self::Iter {
        iter::once(self)
    }
}
impl FsmSequence<Letter> for char {
    type Iter = std::option::IntoIter<Letter>;

    fn into_iter(self) -> Self::Iter {
        Letter::new(self).into_iter()
    }
}
