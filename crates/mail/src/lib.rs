//! Used to send emails.

mod error;

use lettre::{
    message::{header::ContentType, Mailbox, MultiPart, SinglePart},
    transport::smtp::authentication::Credentials,
    AsyncSmtpTransport, AsyncTransport, Message, Tokio1Executor,
};
use std::sync::Arc;

pub use error::{Error, Result};

/// Used to send emails asynchronously.
#[derive(Clone)]
pub struct Mailer {
    mailer: AsyncSmtpTransport<Tokio1Executor>,
    from_mailbox: Arc<Mailbox>,
}
impl Mailer {
    /// Creates a [`Mailer`] using env variables.
    pub fn new(smtp_server: String, email_address: String, email_password: String) -> Result<Self> {
        let from_mailbox = email_address.parse::<Mailbox>()?;
        let credentials = Credentials::new(email_address, email_password);
        let mailer = AsyncSmtpTransport::<Tokio1Executor>::relay(&smtp_server)?
            .credentials(credentials)
            .build();

        Ok(Mailer {
            mailer,
            from_mailbox: Arc::new(from_mailbox),
        })
    }
    /// Sends an email message.
    pub async fn send(
        &self,
        to: &str,
        subject: &str,
        body_html: String,
        body_plain: String,
    ) -> Result<()> {
        let from = (*self.from_mailbox).clone();
        let msg = Message::builder()
            .from(from)
            .to(to.parse()?)
            .subject(subject)
            .multipart(
                MultiPart::alternative()
                    .singlepart(
                        SinglePart::builder()
                            .header(ContentType::TEXT_PLAIN)
                            .body(body_plain),
                    )
                    .singlepart(
                        SinglePart::builder()
                            .header(ContentType::TEXT_HTML)
                            .body(body_html),
                    ),
            )?;

        self.mailer.send(msg).await?;

        Ok(())
    }
}
