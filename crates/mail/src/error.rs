//! Error types for mail module.

pub type Result<T, E = Error> = std::result::Result<T, E>;

pub enum Error {
    EmailAddressParse(lettre::address::AddressError),
    SmtpError(lettre::transport::smtp::Error),
    MessageBuilder(lettre::error::Error),
}

impl From<lettre::address::AddressError> for Error {
    fn from(value: lettre::address::AddressError) -> Self {
        Self::EmailAddressParse(value)
    }
}

impl From<lettre::error::Error> for Error {
    fn from(value: lettre::error::Error) -> Self {
        Self::MessageBuilder(value)
    }
}

impl From<lettre::transport::smtp::Error> for Error {
    fn from(value: lettre::transport::smtp::Error) -> Self {
        Self::SmtpError(value)
    }
}
