//! Stores a `FastFsm` that can be shared across threads.

use crate::error::Result;
use scrabble::util::fsm::FastFsm;
use std::{env, ops::Deref, path::Path, sync::Arc};

/// A structure that contains a thread safe immutable reference
/// to a `scrabble::util::fsm::Fsm` impl.
#[derive(Clone, Debug)]
pub struct FsmHandle(Arc<FastFsm>);
impl FsmHandle {
    /// Loads the Fsm from env variables.
    pub fn from_path(path: &Path) -> Result<Self> {
        log::info!("loading fast fsm: {path}");
        let file = std::fs::File::open(path)?;
        let br = std::io::BufReader::new(file);
        let fast_fsm: FastFsm = bincode::deserialize_from(br)?;

        Ok(Self(Arc::new(fast_fsm)))
    }
}
impl Deref for FsmHandle {
    type Target = FastFsm;

    fn deref(&self) -> &Self::Target {
        self.0.deref()
    }
}
